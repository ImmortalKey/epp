-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 06 août 2021 à 12:28
-- Version du serveur :  8.0.26-0ubuntu0.20.04.2
-- Version de PHP : 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `e_vente`
--

-- --------------------------------------------------------

--
-- Structure de la table `achat`
--

CREATE TABLE `achat` (
  `numach` int NOT NULL,
  `dateach` date DEFAULT NULL,
  `nomcli` varchar(25) DEFAULT NULL,
  `livraison` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE `article` (
  `refact` varchar(10) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `prix` int DEFAULT NULL,
  `codcat` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`refact`, `description`, `prix`, `codcat`) VALUES
('cahchant', 'cahier de chant', 2000, 'desktop'),
('cahpoePor', 'cahier de poesie portable', 2000, 'desktop'),
('cahpoesie', 'cahier de poesie', 2000, 'desktop'),
('DEL30', 'portalble del x300', 100000, 'info'),
('lenovoe550', 'portalble lenovo thinkpad', 200000, 'info'),
('registre', 'livre de registre', 5000, 'desktop'),
('registre1', 'livre de registre portable grand format', 15000, 'desktop'),
('registre10', 'livre de registre grand format', 15000, 'desktop'),
('sax15', 'portalble samsung galaxy s15', 200000, 'info'),
('sax15true', 'portable samsung galaxy s15', 200000, 'info');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `cadcat` varchar(25) NOT NULL,
  `nomcad` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`cadcat`, `nomcad`) VALUES
('cuisine', 'Cuisine'),
('desktop', 'bureautique'),
('info', 'informatiques'),
('sco', 'Scolaires');

-- --------------------------------------------------------

--
-- Structure de la table `detailachat`
--

CREATE TABLE `detailachat` (
  `numach` int NOT NULL,
  `refact` varchar(25) NOT NULL,
  `quantite` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `achat`
--
ALTER TABLE `achat`
  ADD PRIMARY KEY (`numach`);

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`refact`),
  ADD KEY `codcat` (`codcat`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`cadcat`);

--
-- Index pour la table `detailachat`
--
ALTER TABLE `detailachat`
  ADD PRIMARY KEY (`numach`,`refact`),
  ADD KEY `refact` (`refact`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `achat`
--
ALTER TABLE `achat`
  MODIFY `numach` int NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`codcat`) REFERENCES `categorie` (`cadcat`);

--
-- Contraintes pour la table `detailachat`
--
ALTER TABLE `detailachat`
  ADD CONSTRAINT `detailachat_ibfk_1` FOREIGN KEY (`numach`) REFERENCES `achat` (`numach`),
  ADD CONSTRAINT `detailachat_ibfk_2` FOREIGN KEY (`refact`) REFERENCES `article` (`refact`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
